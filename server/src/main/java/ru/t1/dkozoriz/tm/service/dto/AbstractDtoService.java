package ru.t1.dkozoriz.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.dkozoriz.tm.api.service.dto.IAbstractDtoService;
import ru.t1.dkozoriz.tm.dto.model.AbstractModelDto;
import ru.t1.dkozoriz.tm.exception.field.IdEmptyException;

import java.util.List;

public abstract class AbstractDtoService<T extends AbstractModelDto> implements IAbstractDtoService<T> {

    @NotNull
    protected abstract String getName();

    @NotNull
    protected abstract JpaRepository<T, String> getRepository();

    @Override
    @Nullable
    @Transactional
    public T add(@Nullable final T model) {
        if (model == null) return null;
        getRepository().saveAndFlush(model);
        return model;
    }

    @Override
    @Transactional
    public void update(@NotNull final T model) {
        getRepository().saveAndFlush(model);
    }

    @Override
    @Transactional
    public void clear() {
       getRepository().deleteAll();
    }

    @Override
    @NotNull
    public List<T> findAll() {
        return getRepository().findAll();
    }

    @Override
    @Transactional
    public void remove(@Nullable final T model) {
       getRepository().delete(model);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final T model = findById(id);
        if (model == null) return;
        remove(model);
    }

    @Override
    @Nullable
    public T findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        return getRepository().findById(id).orElse(null);
    }

    @Override
    public long getSize() {
        return getRepository().count();
    }

}