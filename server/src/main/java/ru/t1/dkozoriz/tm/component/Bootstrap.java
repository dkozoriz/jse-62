package ru.t1.dkozoriz.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.api.service.IAuthService;
import ru.t1.dkozoriz.tm.api.service.ILoggerService;
import ru.t1.dkozoriz.tm.api.service.IPropertyService;
import ru.t1.dkozoriz.tm.api.service.IServiceLocator;
import ru.t1.dkozoriz.tm.api.service.dto.ISessionDtoService;
import ru.t1.dkozoriz.tm.api.service.dto.IUserDtoService;
import ru.t1.dkozoriz.tm.api.service.dto.business.IProjectDtoService;
import ru.t1.dkozoriz.tm.api.service.dto.business.ITaskDtoService;
import ru.t1.dkozoriz.tm.api.service.model.ISessionService;
import ru.t1.dkozoriz.tm.api.service.model.IUserService;
import ru.t1.dkozoriz.tm.api.service.model.business.IProjectService;
import ru.t1.dkozoriz.tm.api.service.model.business.ITaskService;
import ru.t1.dkozoriz.tm.endpoint.AbstractEndpoint;
import ru.t1.dkozoriz.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@NoArgsConstructor
@Component
public final class Bootstrap implements IServiceLocator {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @Getter
    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Getter
    @NotNull
    @Autowired
    private ITaskDtoService taskDtoService;

    @Getter
    @NotNull
    @Autowired
    private IProjectDtoService projectDtoService;

    @Getter
    @NotNull
    @Autowired
    private ITaskService taskService;

    @Getter
    @NotNull
    @Autowired
    private IProjectService projectService;

    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @Getter
    @NotNull
    @Autowired
    private IUserDtoService userDtoService;

    @Getter
    @NotNull
    @Autowired
    private  IUserService userService;

    @Getter
    @NotNull
    @Autowired
    private  IAuthService authService;

    @Getter
    @NotNull
    @Autowired
    private  ISessionDtoService sessionDtoService;

    @Getter
    @NotNull
    @Autowired
    private  ISessionService sessionService;

    @NotNull
    @Autowired
    private AbstractEndpoint[] endpoints;

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = getPropertyService().getServerHost();
        @NotNull final String port = getPropertyService().getServerPort().toString();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void initEndpoints(@NotNull final AbstractEndpoint[] endpoints) {
        for (@NotNull AbstractEndpoint endpoint : endpoints) {
            registry(endpoint);
        }
    }

    private void prepareStartup() {
        loggerService.info("*** TASK MANAGER SERVER IS STARTED ***");
        initPID();
        initEndpoints(endpoints);
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    private void prepareShutdown() {
        loggerService.info("** TASK MANAGER SERVER IS SHUTTING DOWN **");
    }

    public void run(@Nullable final String... args) {
        prepareStartup();
    }

}