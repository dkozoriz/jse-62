package ru.t1.dkozoriz.tm.repository.model.business;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.dkozoriz.tm.model.business.BusinessModel;
import ru.t1.dkozoriz.tm.repository.model.UserOwnedRepository;

import java.util.List;

@NoRepositoryBean
public interface BusinessRepository<T extends BusinessModel> extends UserOwnedRepository<T> {

    @NotNull
    List<T> findAllByUserId(@NotNull final String userId, @NotNull Sort sort);


}