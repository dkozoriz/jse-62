package ru.t1.dkozoriz.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.dto.request.project.ProjectUpdateByIdRequest;
import ru.t1.dkozoriz.tm.event.ConsoleEvent;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

@Component
public final class ProjectUpdateByIdListener extends AbstractProjectListener {

    public ProjectUpdateByIdListener() {
        super("project-update-by-id", "update project by id.");
    }

    @Override
    @EventListener(condition = "@projectUpdateByIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull ConsoleEvent consoleEvent) {
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        projectEndpoint.projectUpdateById(new ProjectUpdateByIdRequest(getToken(), id, name, description));
    }

}