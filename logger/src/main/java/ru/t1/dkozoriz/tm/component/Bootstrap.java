package ru.t1.dkozoriz.tm.component;

import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.listener.EntityListener;
import ru.t1.dkozoriz.tm.service.LoggerService;

import javax.jms.*;

@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    private EntityListener entityListener;

    @NotNull
    @Autowired
    private ConnectionFactory connectionFactory;

    @NotNull
    private final static String QUEUE = "LOGGER";

    @SneakyThrows
    public void run(@Nullable final String... args) {
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Queue destination = session.createQueue(QUEUE);
        @NotNull final MessageConsumer messageConsumer = session.createConsumer(destination);
        messageConsumer.setMessageListener(entityListener);
    }

}